import { createRouter, createWebHistory } from 'vue-router'
import TodoList from '../views/Todo/TodoList.vue'
import AddTodo from '../views/Todo/AddTodo.vue'
import ViewTodo from '../views/Todo/ViewTodo.vue'
import UpdateTodo from '../views/Todo/UpdateTodo.vue'
import Login from '../views/User/Login.vue'
import Register from '../views/User/Register.vue'
import Welcome from '../views/Todo/Welcome.vue'




const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'welcome',
      component: Welcome,
      meta: {requiresAuth: false}
    },
    {
      path: '/todo',
      name: 'todo',
      component: TodoList,
      meta: {requiresAuth: true}
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {requiresAuth: false}
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {requiresAuth: false}
    },
    {
      path: '/add',
      name: 'add',
      component: AddTodo,
      meta: {requiresAuth: true}

    },
    {
      path: '/update/:id',
      name: 'update',
      component: UpdateTodo,
      meta: {requiresAuth: true}

    },
    {
      path: '/view/:id',
      name: 'view',
      component: ViewTodo,
      meta: {requiresAuth: true}

    },
    

  ]
})
router.beforeEach((to, from, next) => {
  if(to.matched.some((x) => x.meta.requiresAuth)) {
    if(localStorage.getItem('token')) {
      next();
    } else {
    next({name:'login'});
    }
  } else {
    if((to.name == "login" || to.name == "register") && localStorage.getItem('token')) {
    next({name:'todo'});
    } else {
      next();
    }
  }
});

export default router
