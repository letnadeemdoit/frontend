import * as api from "../Services/api"
const state = {
    items:[],
    itemsCount: 0,
    todoItem:{},
}

const mutations = {
    TODO_LIST(state, payload) {
     state.items = payload.data.data;
     state.itemsCount = payload.data.total;
    },
    ADD_TODO(state, payload) {
        console.log(payload);
    },
    UPDATE_TODO(state, payload) {
        state.itemsCount = payload;
    },
    SET_TODO(state, payload) {
        state.todoItem = payload.data;
    },
    DELETE_TODO(state, payload) {
        state.itemsCount = payload;
    }
}

const actions = {
    async ToDoList({ commit }, page) {
        await api.get(`/todolist?page=`+page?.page).then((response) => {
            commit('TODO_LIST', response);
        });
    },
    async AddToDo({ commit }, data) {
        console.log(data);
        await api.post(`/addtodo`,{...data}).then((response) => {
            commit('ADD_TODO', response);
        });
    },
    async EditTodo({ commit }, data) {
        console.log(data);
        await api.post(`/updatetodo`,{...data}).then((response) => {
            commit('EDIT_TODO', response);
        });
    },
    async UpdateToDo({ commit }, data) {
        console.log(data);
        await api.post(`/updatetodo`,{...data}).then((response) => {
            commit('UPDATE_TODO', response);
        });
    },
    async ViewToDo({ commit }, data) {
        await api.get(`/viewtetodo/`+data.id).then((response) => {
            commit('SET_TODO', response);
        });
    },
    async DeleteTodo({ commit }, data) {
        console.log(data);
        await api.deleteAPI(`/deletetetodo/`+data.id).then((response) => {
            commit('DELETE_TODO', response);
        });
    },
    async TodoSearch({ commit}, data) {
        await api.post(`/todosearch`,{search:data.search}).then((response) => {
            commit('TODO_LIST', response);
        }); 
    }
}

const getters = {
    items: ({ items }) => items,
    todoItem: ({ todoItem }) => todoItem,
    itemsCount: ({ itemsCount }) => itemsCount,
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
