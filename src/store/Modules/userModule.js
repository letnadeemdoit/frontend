import * as api from "../Services/api"
const state = {
}

const mutations = {
    USER_LOGIN(state, payload) {
        console.log(payload);
    },
    USER_REGISTER(state, payload) {
        state.itemsCount = payload;
    }
}

const actions = {
    async userRegister({ commit }, data) {
        const instance  = this;
        let status = 0;
        await api.post(`/register`,{...data}).then((response) => {
            if(response.success) {
                localStorage.setItem('token',response.data.token);
            window.location.reload();
            } else if(response.response.data.data == "already") {
                status = 403;
            } 
            
            commit('USER_REGISTER', response);
        });
        return status;
    },
    async userLogin({ commit }, data) {
        console.log(data);
        let status = 0;
        await api.post(`/login`,{...data}).then((response) => {
            if(response.success) {
                localStorage.setItem('token',response.data.token);
                window.location.reload();
            } else if(response.response.data.data == "notexist") {
                status = 403;
            } 
            commit('USER_LOGIN', response);
        });
        return status;
    },
    async userLogout({ commit }) {
        await api.post(`/logout`).then((response) => {
            localStorage.removeItem('token');
            window.location.reload();
            commit('USER_LOGOUT', response);
        });
    },
}

const getters = {
    fields: ({ fields }) => fields,
    items: ({ items }) => items,
    itemsCount: ({ itemsCount }) => itemsCount,
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
