// import * as Vue from "vue"
import * as Vuex from "vuex"

import createPersistedState from "vuex-persistedstate"

// Vue.use(Vuex);

import todoModule from "./Modules/todoModule"
import userModule from "./Modules/userModule"


const state = {}

const mutations = {}

const actions = {}

export default new Vuex.Store({
    modules: {
        todoModule: todoModule,
        userModule: userModule
    },
    state,
    mutations,
    actions,
    plugins: [
        createPersistedState({
            storage: window.sessionStorage,
        })
    ]
})
