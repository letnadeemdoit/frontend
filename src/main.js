import Axios from "axios"
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import * as Vuex from "vuex"
import 'bootstrap/dist/css/bootstrap.css'

import './assets/main.css'
import store from "./store/index"
import BalmUI from 'balm-ui'; // Official Google Material Components
import BalmUIPlus from 'balm-ui/dist/balm-ui-plus'; // BalmJS Team Material Components
import 'balm-ui-css';

const app = createApp(App)

app.use(BalmUI); // Mandatory
app.use(BalmUIPlus); // Optional
app.use(router)
app.use(store);

app.config.productionTip = false
app.config.globalProperties.$http = Axios
app.config.globalProperties.$http.defaults.headers.post['Accept'] = 'application/json'
app.config.globalProperties.$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

app.config.globalProperties.$http.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
app.config.globalProperties.$http.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
app.config.globalProperties.$http.defaults.headers.common['Access-Control-Allow-Headers'] = '*'

const token = localStorage.getItem('token');
if (token) {
    app.config.globalProperties.$http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  }
app.use(Vuex);

app.mount('#app')
